FROM ubuntu:latest

LABEL org.opencontainers.image.authors="Andrew Kluev <kluev.andrew@gmail.com>"

WORKDIR /tmp

RUN apt-get update \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        wget \
        bash \
        iputils-ping \
        tar \
        git \
        build-essential \
        libgoogle-perftools-dev \
        libsnappy-dev \
        libleveldb-dev \
        unzip \
        pkg-config \
    && wget https://golang.org/dl/go1.17.5.linux-amd64.tar.gz \
    && tar -xzf go1.17.5.linux-amd64.tar.gz -C /usr/local/ \
    && ln -s /usr/local/go/bin/go /usr/local/bin/go \
    && go version \
    && curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.43.0 \
    && ln -s /root/go/bin/golangci-lint /usr/local/bin/golangci-lint \
    && git clone https://github.com/Restream/reindexer.git \
    && cd reindexer \
    && git checkout v4.0.0 \
    && ./dependencies.sh \
    && mkdir build \
    && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Release .. \
    && make \
    && make install \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/reindexer
