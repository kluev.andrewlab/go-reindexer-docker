#!/usr/bin/env bash
set -e

if ! [ "$AGE_KEY_B64" = '' ]; then
  echo "${AGE_KEY_B64}" | base64 -d > "${SOPS_AGE_KEY_FILE}"
fi

if [[ -d ${1} ]]; then
  set -- sopsctl "$@"
fi

exec "$@"
